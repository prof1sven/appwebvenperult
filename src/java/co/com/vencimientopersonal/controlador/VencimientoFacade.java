/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.vencimientopersonal.controlador;

import co.com.vencimientopersonal.entidades.Vencimiento;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Alejandro Tamayo
 */
@Stateless
public class VencimientoFacade extends AbstractFacade<Vencimiento> {

    @PersistenceContext(unitName = "vencimiento_personalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public VencimientoFacade() {
        super(Vencimiento.class);
    }
    
}
