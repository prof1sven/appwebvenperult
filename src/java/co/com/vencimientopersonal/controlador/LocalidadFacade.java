/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.vencimientopersonal.controlador;

import co.com.vencimientopersonal.entidades.Localidad;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Alejandro Tamayo
 */
@Stateless
public class LocalidadFacade extends AbstractFacade<Localidad> {

    @PersistenceContext(unitName = "vencimiento_personalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public LocalidadFacade() {
        super(Localidad.class);
    }
    
        
    
    public List<Localidad> obtenerDepartamentos(){
        //sql nativo
        Query q=em.createNativeQuery("select codigo_localidad, nombre, longitud, latitud"
                + " from public.localidad where length(codigo_localidad)=5 order by 2",Localidad.class);
        
        return q.getResultList();
    }   
    
    public List<Localidad> obtenerMunicipio(){
        //sql nativo
        Query q=em.createNativeQuery("select codigo_localidad, nombre, longitud, latitud"
                + " from public.localidad where length(codigo_localidad)=8 order by 2",Localidad.class);
        
        return q.getResultList();
    } 
}
