/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.vencimientopersonal.controlador;

import co.com.vencimientopersonal.controlador.util.JsfUtil;
import co.com.vencimientopersonal.entidades.Usuario;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.ejb.EJB;

/**
 *
 * @author Alejandro Tamayo
 */
@Named(value = "loginController")
@SessionScoped
public class LoginController implements Serializable {

    private String nombreUsuario = "";
    private String passwordUsuario = "";
    @EJB
    private UsuarioFacade usuFacade;

    /**
     * Creates a new instance of LoginController
     */
    public LoginController() {

    }

    public String ingresar() {
        Usuario usuEncontrado = usuFacade.obtenerUsuarioxEmail(this.nombreUsuario);
        if (usuEncontrado != null) {
            if (usuEncontrado.getPassword().compareTo(passwordUsuario) == 0) {
                return "acceder";
            }
            JsfUtil.addErrorMessage("Contraseña erronea");
        } else {
            JsfUtil.addErrorMessage("El usuario no existe");
        }
        return null;

    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getPasswordUsuario() {
        return passwordUsuario;
    }

    public void setPasswordUsuario(String passwordUsuario) {
        this.passwordUsuario = passwordUsuario;

    }
}
