/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.vencimientopersonal.controlador;

import co.com.vencimientopersonal.entidades.Usuario;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Alejandro Tamayo
 */
@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> {

    @PersistenceContext(unitName = "vencimiento_personalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }
    
    public Usuario obtenerUsuarioxEmail( String correo){
        Query q= em.createNamedQuery("Usuario.findByEmail", Usuario.class).setParameter("email", correo);
        
        List<Usuario> lista= q.getResultList();
        if (lista.isEmpty()){
            return null;
        }else{
            return lista.get(0);
        }
    }
}
