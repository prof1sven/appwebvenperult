/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.vencimientopersonal.controlador;

import co.com.vencimientopersonal.entidades.Localidad;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author Alejandro Tamayo
 */
@Named(value = "parametricasBean")
@ApplicationScoped
public class ParametricasBean {
    
    private List<Localidad> Depar;
    
    private List<Localidad> Municipio;
    @EJB
    private LocalidadFacade conLocalidad;

    /**
     * Creates a new instance of ParametricasBean
     */
    
    private String localidadSeleccionada=""; 
    public ParametricasBean() {
    }
    @PostConstruct
    public void inicializar(){
        Depar= new ArrayList<>();
       
        Depar=conLocalidad.obtenerDepartamentos();
        Municipio=conLocalidad.obtenerMunicipio();
        
    }

    public List<Localidad> getDepar() {
        return Depar;
    }

    public void setDepar(List<Localidad> Depar) {
        this.Depar = Depar;
    }

   

    public List<Localidad> getMunicipio() {
        
        List<Localidad> muniTemp= new ArrayList<>();
        for(Localidad loc: Municipio){
            if(loc.getCodigoLocalidad().startsWith(localidadSeleccionada)){
                muniTemp.add(loc);
            }
            
                    
        }
        return muniTemp;
    }

    public void setMunicipio(List<Localidad> Municipio) {
        this.Municipio = Municipio;
    }

    public String getLocalidadSeleccionada() {
        return localidadSeleccionada;
    }

    public void setLocalidadSeleccionada(String localidadSeleccionada) {
        this.localidadSeleccionada = localidadSeleccionada;
    }
    
    
}
