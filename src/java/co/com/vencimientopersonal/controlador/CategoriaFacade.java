/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.vencimientopersonal.controlador;

import co.com.vencimientopersonal.entidades.Categoria;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Alejandro Tamayo
 */
@Stateless
public class CategoriaFacade extends AbstractFacade<Categoria> {

    @PersistenceContext(unitName = "vencimiento_personalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CategoriaFacade() {
        super(Categoria.class);
    }

    public List obtenerCantidadContactoxCategoria() {
        Query q = em.createNativeQuery("select tipo_contacto.nombre_tipo_contacto AS \"Tipo de contactos\" , count(contacto.id_contacto) as \"Contactos\" from tipo_contacto\n"
                + "join contacto on contacto.tipo_contacto=tipo_contacto.id_tipo_contacto\n"
                + "group by(tipo_contacto.nombre_tipo_contacto)");
        List<Object[]> lista=q.getResultList();
        return lista;
    }
    public List obtenerCantidadVencimientoxCategoria() {
        Query q = em.createNativeQuery("select categoria.nombre_categoria AS \"Tipo de contactos\" , count(vencimiento.id_vencimiento) as \"Vencimientos\" from categoria\n" +
"                join vencimiento on categoria.id_categoria=vencimiento.categoria\n" +
"                group by(categoria.nombre_categoria)");
        List<Object[]> lista=q.getResultList();
        return lista;
    }
    
}
